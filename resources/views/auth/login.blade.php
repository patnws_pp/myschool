@extends('layouts.login') @section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="#"><i class="fa fa-bank"></i> <b>My</b>School</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <h4 class="login-box-msg"><b>MySchool</b> Login</h4>

        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
            @csrf
            <div class="form-group has-feedback">
                <input id="email" type="text" class="form-control" placeholder="ชื่อผู้ใช้" name="user" value="{{ old('email') }}" required autofocus>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input id="password" type="password" class="form-control" name="password" required placeholder="รหัสผ่าน">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
            <div class="col-xs-8">
            <div class="checkbox iCheck">
            <label>
              <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} > Remember Me
            </label>
            </div>
            </div>
                <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">เข้าสู่ระบบ</button>
            </div>
            </div>
            @if ($errors->has('user'))
            <div class="alert alert-danger alert-dismissible">
                <h4><i class="icon fa fa-ban"></i> ล็อกอินผิดพลาด!</h4>
               {{ $errors->first('user') }}
              </div>
             @endif
        </form>
        <hr>
        <p class="login-box-msg">Copyright © 2018 MySchool V.1 Beta</p>
    </div>
    
</div>
<!-- /.login-box -->

@endsection
